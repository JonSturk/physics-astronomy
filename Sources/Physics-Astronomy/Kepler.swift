//
//  Kepler.swift
//  WBSimulation
//
//  Created by Jon Sturk on 2016-09-16.
//  Copyright © 2016 Jon Sturk. All rights reserved.
//

import Foundation
//import JSCommon
import Math
import simd

public struct Physics {
	static let G = 6.674e-11
}

public struct OrbitalPosition {
	let orbit: KeplerOrbit
	let time: UInt
	let last: Double
	
	public init(orbit o: KeplerOrbit, time t: UInt, last l: Double = 0.0) {
		self.orbit = o
		self.time = t
		self.last = l
	}
	
	public var parentOrbit: OrbitalPosition? {
		guard let po = orbit.majorBody.parentOrbit else {
			return nil
		}
		return OrbitalPosition(orbit: po, time: time)
	}
	
	public lazy var meanAnomaly: Double = {
		return orbit.meanAnomaly(time: time)
	}()
	
	public lazy var eccentricAnomaly: Double = {
		return orbit.keplerSolve(meanAnomaly: meanAnomaly, initialValue: last)
	}()
	
	public lazy var trueAnomaly: Angle<Double> = {
		return Angle(radians: orbit.convertToTrueAnomaly(eccentricAnomaly: eccentricAnomaly))
	}()
	
	public lazy var distance: Distance<Double> = {
		return Distance(m: orbit.distance(eccentricAnomaly: eccentricAnomaly))
	}()
	
	public lazy var state: (r: double3, v: double3) = {
		orbit.stateAt(trueAnomaly: trueAnomaly)
	}()
	
	public lazy var worldPosition: double4 = {
		var local = double4(xyz: state.r, w: 1.0)
		if var po = parentOrbit {
			return po.worldMatrix * local
		}
		
		return local
	}()
	
	public lazy var worldMatrix: double4x4 = {
		let mat = double4x4(translation: state.r) * orbit.eulerMatrix
		if var po = parentOrbit {
			return po.worldMatrix * mat
		}
		return mat
	}()
}

public struct KeplerOrbit: Hashable, CustomStringConvertible {
	public let majorBody: OrbitalObject
	public let minorBody: OrbitalObject
	
	public var inclination: Angle<Double>
	public var rightAscension: Angle<Double>
	
	public func declination(atTime time: UInt) -> Double {
		let pol = orbitalPosition(atTime: time)
		let ep = JSPlane<Double>(pointOnPlane: cartesianPosition(atTime: time), planeNormal: minorBody.axis)
		let h = ep.distance(toPoint: double3(0, 0, 0))
		
		return -asin(h / pol.distance)
	}
	
	public var description: String {
		return "\(majorBody.description): \(minorBody.description)"
	}
	
	public func hash(into hasher: inout Hasher) {
		hasher.combine(description)
	}
	
	internal var a: Double {
		return orbit.semiMajorAxis
	}
	
	public var eccentricity: Double {
		return orbit.eccentricity
	}
	
	public var gravitationalParameter: Double {
		return Physics.G * (majorBody.mass + minorBody.mass)
	}
	
	public let orbit: Elipse<Double>
	
	public init(majorBody: OrbitalObject, minorBody: OrbitalObject, semiMajorAxis a: Double, eccentricity e: Double, inclination i: Angle<Double> = Angle(radians: 0.0), rightAscension ra: Angle<Double> = Angle(radians: 0.0), arguementOfPerigee aop: Angle<Double> = Angle(radians: 0.0)) {
		self.majorBody = majorBody
		self.minorBody = minorBody
		self.inclination = i
		self.rightAscension = ra
		self.argumentOfPerigee = aop
		
		let i = inclination.radians
		let RA = rightAscension.radians
		let w = argumentOfPerigee.radians
		
		eulerMatrix = double4x4(columns: (double4(-sin(RA)*cos(i)*sin(w)+cos(RA)*cos(w), -sin(RA)*cos(i)*cos(w)-cos(RA)*sin(w),  sin(RA)*sin(i), 0.0),
										  double4( cos(RA)*cos(i)*sin(w)+sin(RA)*cos(w),  cos(RA)*cos(i)*cos(w)-sin(RA)*sin(w), -cos(RA)*sin(i), 0.0),
										  double4( sin(i)*sin(w), sin(i)*cos(w), cos(i), 0.0),
										  double4(0.0, 0.0, 0.0, 1.0)))
		
		
		orbit = Elipse(semiMajorAxis: a, eccentricity: e)
	}
	
	public var averageDistance: Double {
		return sqrt(orbit.semiMajorAxis * orbit.semiMinorAxis)
	}
	
	public var periapsis: Double {
		return distance(eccentricAnomaly: 0.0)
	}
	
	public var argumentOfPerigee: Angle<Double>
	
	public var apoapsis: Double {
		return distance(eccentricAnomaly: Double.pi)
	}
	
	public var angularMomentum: Double {
		let mu = Physics.G * (majorBody.mass + minorBody.mass)
		let e_ratio = 1.0 / (1.0 + orbit.eccentricity)
		
		return sqrt(periapsis * mu / e_ratio)
	}
	
	public var period: Double { //Time in seconds
		return 2 * Double.pi * sqrt((a * a * a) / (Physics.G * majorBody.mass))
	}
	
	public func velocity(distance r: Double) -> Double {
		return sqrt(Physics.G * majorBody.mass * ((2 / r) - (1 / a)))
	}
	
	public func meanAnomaly(time: UInt, startingTime: UInt = 0) -> Double {
		let n = (2 * Double.pi) / period
		return n * Double(time - startingTime)
	}
	
	public func convertToTrueAnomaly(eccentricAnomaly E: Double) -> Double {
		let ta = acos((cos(E) - orbit.eccentricity) / (1 - orbit.eccentricity * cos(E)))
		if E >= Double.pi {
			return 2 * Double.pi - ta
		} else {
			return ta
		}
	}
	
	internal func keplerStep(meanAnomaly M: Double, x: Double) -> Double {
		let e = orbit.eccentricity
		let t1 = cos(x)
		let t2 = -1 + e * t1
		let t3 = sin(x)
		let t4 = e * t3
		let t5 = -x + t4 + M
		let t6 = t5 / (1 / 2 * t5 * t4 / t2 + t2 )
		return t5 / ((1 / 2 * t3 - 1 / 6 * t1 * t6) * e * t6 + t2)
	}
	
	public func keplerSolve(meanAnomaly M: Double, tolerance tol: Double = 1e-14, initialValue: Double = 0.0) -> Double {
		let Mn = fmod(M, 2 * Double.pi)
		
		var E = 0.0
		var E0 = initialValue
		var dE = tol + 1.0
		var count = 0
		
		while dE > tol {
			E = E0 - keplerStep(meanAnomaly: Mn, x: E0)
			dE = abs(E - E0)
			E0 = E
			
			count += 1
			if count > 100 {
				fatalError("Failed to converge.")
			}
		}
		//print("Kepler converged after \(count) iterations")
		return E
	}
	
	public func distance(eccentricAnomaly: Double) -> Double {
		return orbit.semiMajorAxis * (1 - orbit.eccentricity * cos(eccentricAnomaly))
	}
	
	//We're returning the eccentric anomaly here to be used as a guess for the next step.
	public func orbitalPosition(atTime time: UInt, guess: Double = 0.0) -> (trueAnomaly: Double, distance: Double, eccentricAnomaly: Double) {
		let mean = meanAnomaly(time: time)
		let eccentric = keplerSolve(meanAnomaly: mean, initialValue: guess)
		
		let dist = distance(eccentricAnomaly: eccentric)
		
		let ang = convertToTrueAnomaly(eccentricAnomaly: eccentric)
		
		return (ang, dist, eccentric)
	}
	
	public func stateAt(trueAnomaly: Angle<Double>) -> (r: double3, v: double3) {
		let h = angularMomentum
		let TA = trueAnomaly.radians
		let e = eccentricity
		let mu = gravitationalParameter
		
		let r = ((h * h / mu) * (1.0 / (1.0 + e * cos(TA))) * double4(cos(TA), sin(TA), 0, 1.0))
		let v = (mu / h) * double4(-sin(TA), e + cos(TA), 0, 1.0)
		
		let em = self.eulerMatrix
		return ((em * r).xyz, (em * v).xyz)
	}
	
	public func position(trueAnomaly: Angle<Double>) -> double3 {
		let h = angularMomentum
		let TA = trueAnomaly.radians
		let e = eccentricity
		let mu = gravitationalParameter
		
		let r = ((h * h / mu) * (1.0 / (1.0 + e * cos(TA))) * double4(cos(TA), sin(TA), 0, 1.0))
		
		//		print("h = \(h), mu = \(mu), e = \(e)")
		
		return r.xyz
	}
	
	public var eulerMatrix: double4x4
	
	//Even though this returns a coordinate in R3, the results will always lie on the system's orbital plane.
	public func cartesianPosition(atTime time: UInt) -> double3 {
		let polar = orbitalPosition(atTime: time)
		return double3(x: polar.distance * cos(polar.trueAnomaly), y: polar.distance * sin(polar.trueAnomaly), z: 0.0)
	}
}
