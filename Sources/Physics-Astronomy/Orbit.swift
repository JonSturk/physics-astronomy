//
//  Universe.swift
//  SceneApp
//
//  Created by Jon Sturk on 2018-09-10.
//  Copyright © 2018 Jon Sturk. All rights reserved.
//

import Foundation
import Math
import simd

public func == (lhs: OrbitalObject, rhs: OrbitalObject) -> Bool {
	return lhs.name == rhs.name && lhs.mass == rhs.mass
}

public struct OrbitalObject: Equatable, CustomStringConvertible {
	public enum Property {
		case Spherical(radius: Double), Ellipsoid(radii: double3) //Unit: meters
	}
	static func === (lhs: OrbitalObject, rhs: OrbitalObject) -> Bool {
		return lhs.storage === rhs.storage
	}
	class Storage {
		var parentOrbit: KeplerOrbit?
		var name: String
		var mass: Double
		var axis: double3
		var rotationTime: Double
		var childOrbits: [KeplerOrbit]
		var properties: [Property]
		
		init(name n: String, mass m: Double, parentOrbit p: KeplerOrbit? = nil, axis: double3 = [0, 0, 0], rotationTime rt: Double = 0.0, childOrbits co: [KeplerOrbit] = [], properties prop: [Property] = []) {
			self.name = n
			self.mass = m
			self.parentOrbit = p
			self.axis = axis
			self.rotationTime = rt
			self.childOrbits = co
			self.properties = prop
		}
		func copy() -> Storage {
			return Storage(name: name, mass: mass, parentOrbit: parentOrbit, axis: axis, rotationTime: rotationTime, childOrbits: childOrbits, properties: properties)
		}
	}
	
	private var storage: Storage
	
	public init(name n: String? = nil, mass m: Double, parentOrbit p: KeplerOrbit? = nil, axis: double3 = [0, 0, 0], rotationTime rt: Double = 0.0, properties prop: [Property] = []) {
		let name = n ?? String.init(describing: Int.random(in: Int.min ... Int.max))
		storage = Storage(name: name, mass: m, parentOrbit: p, axis: axis, rotationTime: rt, properties: prop)
	}
	
	public var description: String {
		return storage.name
	}
	
	public var parentOrbit: KeplerOrbit? {
		get {
			return storage.parentOrbit
		}
		set(newVal) {
			if isKnownUniquelyReferenced(&storage) {
				storage.parentOrbit = newVal
			} else {
				let copied = storage.copy()
				copied.parentOrbit = newVal
				storage = copied
			}
		}
	}
	
	public var childOrbits: [KeplerOrbit] {
		get {
			return storage.childOrbits
		}
		set(newVal) {
			if isKnownUniquelyReferenced(&storage) {
				storage.childOrbits = newVal
			} else {
				let copied = storage.copy()
				copied.childOrbits = newVal
				storage = copied
			}
		}
	}
	
	public var properties: [Property] {
		get {
			return storage.properties
		}
		set(newVal) {
			if isKnownUniquelyReferenced(&storage) {
				storage.properties = newVal
			} else {
				let copied = storage.copy()
				copied.properties = newVal
				storage = copied
			}
		}
	}
	
	public mutating func addChildOrbit(_ child: inout OrbitalObject, semimajorAxis a: Distance<Double>, eccentricity e: Double, inclination i: Angle<Double>, rightAscension ra: Angle<Double>, argumentOfPerigee aop: Angle<Double>) {
		let orbit = KeplerOrbit(majorBody: self, minorBody: child, semiMajorAxis: a.m, eccentricity: e, inclination: i, rightAscension: ra, arguementOfPerigee: aop)
		child.parentOrbit = orbit
		var co = childOrbits
		co.append(orbit)
		childOrbits = co
		
	}
	
	public var name: String {
		get {
			return storage.name
		}
		set(newVal) {
			if isKnownUniquelyReferenced(&storage) {
				storage.name = newVal
			} else {
				let copied = storage.copy()
				copied.name = newVal
				storage = copied
			}
		}
	}
	
	public var mass: Double {
		get {
			return storage.mass
		}
		set(newVal) {
			if isKnownUniquelyReferenced(&storage) {
				storage.mass = newVal
			} else {
				let copied = storage.copy()
				copied.mass = newVal
				storage = copied
			}
		}
	}
	
	public var axis: double3 {
		get {
			return storage.axis
		}
		set(newVal) {
			if isKnownUniquelyReferenced(&storage) {
				storage.axis = newVal
			} else {
				let copied = storage.copy()
				copied.axis = newVal
				storage = copied
			}
		}
	}
	
	public var rotationTime: Double {
		get {
			return storage.rotationTime
		}
		set(newVal) {
			if isKnownUniquelyReferenced(&storage) {
				storage.rotationTime = newVal
			} else {
				let copied = storage.copy()
				copied.rotationTime = newVal
				storage = copied
			}
		}
	}
}

func orbitalElements(r: SIMD3<Double>, v: SIMD3<Double>, frame: (I: SIMD3<Double>, J: SIMD3<Double>, K: SIMD3<Double>), gravitaionalParameter mu: Double) -> (angularMomentum: SIMD3<Double>, inclination: Angle<Double>, rightAscension: Angle<Double>, e: SIMD3<Double>, perigee: Angle<Double>, trueAnomaly: Angle<Double>, semimajorAxis: Double) {
	let r_norm = length(r)
	let v_norm = length(v)
	
	let v_r = dot(r, v) / r_norm
	
	let h = cross(r, v)
	let h_norm = length(h)
	
	let i = acos(h.z / h_norm)
	
	let N = cross(frame.K, h)
	
	let N_norm = length(N)
	
	var ohm = acos(N.x / N_norm)
	if N.y < 0 {
		ohm = 2 * Double.pi - ohm
	}
	
	let e: double3 = (1.0 / mu) * (((v_norm * v_norm - (mu / r_norm)) * r) - r_norm * v_r * v)
	let e_norm = length(e)
	
	var omega = acos(dot(N / N_norm, e / e_norm))
	if e.z < 0 {
		omega = 2 * Double.pi - omega
	}
	
	var theta = acos(dot(e / e_norm, r / r_norm))
	if v_r < 0 {
		theta = 2 * Double.pi - theta
	}
	
	let a = (h_norm * h_norm / mu) / (1.0 - e_norm * e_norm)
	
	
	return (h, Angle(radians: i), Angle(radians: ohm), e, Angle(radians: omega), Angle(radians: theta), a)
}

public func stateVector(gravitationalParameter mu: Double, angularMomentum h: Double, inclination: Angle<Double>, eccentricity e: Double, rightAscension: Angle<Double>, argumentOfPeriapsis: Angle<Double>, trueAnomaly: Angle<Double>) -> (r: SIMD3<Double>, v: SIMD3<Double>) {
	
	let i = inclination.radians
	let RA = rightAscension.radians
	let w = argumentOfPeriapsis.radians
	let TA = trueAnomaly.radians
	
	let r = (h * h / mu) * (1.0 / (1.0 + e * cos(TA))) * double3(cos(TA), sin(TA), 0)
	let v = (mu / h) * double3(-sin(TA), e + cos(TA), 0)
	
	let R = double3x3(columns: (SIMD3<Double>(-sin(RA)*cos(i)*sin(w)+cos(RA)*cos(w), -sin(RA)*cos(i)*cos(w)-cos(RA)*sin(w), sin(RA)*sin(i)),
								SIMD3<Double>(cos(RA)*cos(i)*sin(w)+sin(RA)*cos(w), cos(RA)*cos(i)*cos(w)-sin(RA)*sin(w), -cos(RA)*sin(i)),
								SIMD3<Double>(sin(i)*sin(w), sin(i)*cos(w), cos(i))))
	
	let R_inv = R.transpose
	
	return (R_inv * r, R_inv * v)
}
