// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Physics-Astronomy",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "Physics-Astronomy",
            targets: ["Physics-Astronomy"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
		.package(url: "https://JonSturk@bitbucket.org/JonSturk/common-math.git", .branch("master")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "Physics-Astronomy",
            dependencies: ["Math"]),
        .testTarget(
            name: "Physics-AstronomyTests",
            dependencies: ["Physics-Astronomy"]),
    ]
)
