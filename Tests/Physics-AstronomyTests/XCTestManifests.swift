import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(Physics_AstronomyTests.allTests),
    ]
}
#endif
