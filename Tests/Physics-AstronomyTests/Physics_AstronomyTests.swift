import XCTest
@testable import Physics_Astronomy

final class Physics_AstronomyTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Physics_Astronomy().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
