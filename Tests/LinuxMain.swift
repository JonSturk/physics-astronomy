import XCTest

import Physics_AstronomyTests

var tests = [XCTestCaseEntry]()
tests += Physics_AstronomyTests.allTests()
XCTMain(tests)
